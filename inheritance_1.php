<?php

class BITM{
    public $window;//this is properties or variable
    public $door;//this is properties or variable
    public $table;//this is properties or variable
    public $chair;//this is properties or variable
    public $whiteboard;//this is properties or variable

    public function coolTheAir(){ //this is method . this name can't be noun,can be verb
        echo "I'm cooling the air";
    }
    public function compute(){ //this is method, this name can't be noun,can be verb
        echo "I'm computing";
    }
    public function show(){ //this is method, this name can't be noun,can be verb
        echo $this->window."<br>";
        echo $this->door."<br>";
        echo $this->chair."<br>";
        echo $this->table."<br>";
        echo $this->whiteboard."<br>";
    }

    public function setWindow($win)//here $win is local
    {
        $this->window = $win;//here $this->window is global & =$wind is local, $this is pointer . $this->window access public $window .
    }
    public function setDoor($dr)
    {
        $this->door = $dr;
    }
    public function setChair($chr)
    {
        $this->chair = $chr;
    }
    public function setTable($tab)
    {
        $this->table = $tab;
    }
    public function setWhiteboard($board)
    {
        $this->whiteboard = $board;
    }

    public function setAll(){
        $this->setWindow("I'm a window");//here setAll method is in BITM property & stWindow method also is in BITM property
        $this->setDoor("I'm a door");
        $this->setChair("I'm a chair");
        $this->setTable("I'm a table");
        $this->setWhiteboard("I'm a whiteboard");

    }
}//end of class BITM

$obj_BITM_at_Ctg = new BITM;//suppose this line is used for constructing a building
$obj_BITM_at_Ctg->setAll();
$obj_BITM_at_Ctg->show();

class BITM_Lab402 extends BITM{//It is a child class of class BITM

}

$objBITM_Lab = new BITM_Lab402();
$objBITM_Lab->setAll();
$objBITM_Lab->show();

?>