<?php

class BITM{
    public $window;//this is properties or variable
    public $door;//this is properties or variable
    public $table;//this is properties or variable
    public $chair;//this is properties or variable
    public $whiteboard;//this is properties or variable

    public function coolTheAir(){ //this is method . this name can't be noun,can be verb
        echo "I'm cooling the air";
    }
    public function compute(){ //this is method, this name can't be noun,can be verb
        echo "I'm computing";
    }
    public function show(){ //this is method, this name can't be noun,can be verb
        echo $this->window."<br>";
        echo $this->door."<br>";
        echo $this->chair."<br>";
        echo $this->table."<br>";
        echo $this->whiteboard."<br>";
    }

    public function setWindow($win)//here $win is local
    {
        $this->window = $win;//here $this->window is global & =$wind is local, $this is pointer . $this->window access public $window .
    }
    public function setDoor($dr)
    {
        $this->door = $dr;
    }
    public function setChair($chr)
    {
        $this->chair = $chr;
    }
    public function setTable($tab)
    {
        $this->table = $tab;
    }
    public function setWhiteboard($board)
    {
        $this->whiteboard = $board;
    }

}//end of class BITM

$obj_BITM_at_Ctg = new BITM;//suppose this line is used for constructing a building

$obj_BITM_at_Ctg->setWindow("I'm a window");
$obj_BITM_at_Ctg->setDoor("I'm a door");
$obj_BITM_at_Ctg->setChair("I'm a chair");
$obj_BITM_at_Ctg->setTable("I'm a table");
$obj_BITM_at_Ctg->setWhiteboard("I'm a whiteboard");
$obj_BITM_at_Ctg->show();

class BITM_Lab402 extends BITM{//It is a child class of class BITM

}

$objBITM_Lab=new BITM_Lab402();

$objBITM_Lab->setWindow("this is a window");
$objBITM_Lab->setDoor("This is a door");
$objBITM_Lab->setChair("This is a chair");
$objBITM_Lab->setTable("This a table");
$objBITM_Lab->setWhiteboard("This is a whiteboard");

$objBITM_Lab->show();

?>